import liblo
from time import time


server = liblo.Server(23456)
messages = 0
processed_messages = 0
last_time = time()


def test_handler(path, args, types, src):
    global messages
    global processed_messages
    global last_time
    messages += 1
    if (time() - last_time > 1):
        print(f"messages received :-> {messages - processed_messages}")
        processed_messages = messages
        last_time = time()


server.add_method("/test", None, test_handler)


while True:
    server.recv(1)

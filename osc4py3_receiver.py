# Import needed modules from osc4py3
from osc4py3.as_eventloop import *
from osc4py3 import oscmethod as osm
from time import sleep, time


messages = 0
processed_messages = 0
last_time = time()


def handler(*args):
    global messages
    global processed_messages
    global last_time
    messages += 1
    if (time() - last_time > 1):
        print(f"messages received :-> {messages - processed_messages}")
        processed_messages = messages
        last_time = time()


# Start the system.
osc_startup()

# Make server channels to receive packets.
osc_udp_server("127.0.0.1", 23456, "aservername")

# Associate Python functions with message address patterns, using default
# argument scheme OSCARG_DATAUNPACK.
osc_method("/test", handler)

# Periodically call osc4py3 processing method in your event loop.
finished = False
while not finished:
    # …
    osc_process()
    # …

# Properly close the system.
osc_terminate()

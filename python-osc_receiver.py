"""Small example OSC server

This program listens to several addresses, and prints some information about
received packets.
"""
import argparse
import math
from time import sleep, time

from pythonosc import dispatcher
from pythonosc import osc_server


messages = 0
processed_messages = 0
last_time = time()


def callback(unused_addr, *args):
    global messages
    global processed_messages
    global last_time
    messages += 1
    if (time() - last_time > 1):
        print(f"messages received :-> {messages - processed_messages}")
        processed_messages = messages
        last_time = time()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip",
                        default="127.0.0.1", help="The ip to listen on")
    parser.add_argument("--port",
                        type=int, default=23456, help="The port to listen on")
    args = parser.parse_args()

    dispatcher = dispatcher.Dispatcher()
    dispatcher.map("/test", callback)

    server = osc_server.ThreadingOSCUDPServer(
        (args.ip, args.port), dispatcher)
    print("Serving on {}".format(server.server_address))
    server.serve_forever()

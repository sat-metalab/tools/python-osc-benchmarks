#include <iostream>
#include <lo/lo.h>
#include <lo/lo_cpp.h>


int main(){
  lo::Address test("localhost", 23456);
  int message = 0;
  while(true) {
    std::cout << "Message out!" << std::endl;
    test.send("/test", "fffsi", 1.0, 2.0, 3.0, "test", ++message);
  }
}

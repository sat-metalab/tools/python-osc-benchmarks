from oscpy.server import OSCThreadServer
from time import sleep, time


messages = 0
processed_messages = 0
last_time = time()


def callback(*args):
    global messages
    global processed_messages
    global last_time
    messages += 1
    if (time() - last_time > 1):
        print(f"messages received :-> {messages - processed_messages}")
        processed_messages = messages
        last_time = time()


osc = OSCThreadServer()  # See sources for all the arguments

# You can also use an \*nix socket path here
sock = osc.listen(address='0.0.0.0', port=23456, default=True)
osc.bind(b'/test', callback)
sleep(10)
osc.stop()  # Stop the default socket

osc.stop_all()  # Stop all sockets

# Here the server is still alive, one might call osc.listen() again

osc.terminate_server()  # Request the handler thread to stop looping

osc.join_server()  # Wait for the handler thread to finish pending tasks and exit
